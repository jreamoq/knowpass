<?php
class STResort{
	public function __construct() {
		add_action('wp_ajax_st_get_resort_by_location', array($this, '__getResortByLocationID'));
		add_action('wp_ajax_nopriv_st_get_resort_by_location', array($this, '__getResortByLocationID'));
        add_action('admin_footer',array($this,'_add_js_tmpl'));

        add_action('wp_ajax_st_get_resort_price_ajax', array($this, '__getResortPriceAjax'));
        add_action('wp_ajax_nopriv_st_get_resort_price_ajax', array($this, '__getResortPriceAjax'));

		add_action('woocommerce_after_order_notes', array($this, 'customise_checkout_field'));

		//add_action('woocommerce_checkout_process', array($this, 'customise_checkout_field_process'));
		add_action('woocommerce_checkout_update_order_meta', array($this, 'customise_checkout_field_update_order_meta'));
	}

	function customise_checkout_field_update_order_meta($order_id)
	{
		if (!empty($_POST['st_guest'])) {
			$has_guest_card = STInput::post('check_has_guest', 'yes');
			update_post_meta( $order_id, 'has_guest_card',  $has_guest_card);
			$guests = STCart::renderGuestCard();
			update_post_meta( $order_id, 'guest_card',  $guests);

		    $cart_item = STCart::get_carts();
			$cart_temp = array_shift($cart_item);
		    if(!empty($cart_temp['data'])){
		        foreach ($cart_temp['data'] as $k => $v){
			        update_post_meta( $order_id, 'stw_' .$k ,  $v);
                }
            }
		}
		$order = wc_get_order( $order_id );
		STCart::createQRCodeImage($order_id, $order->data['payment_method_title']);
	}

	function customise_checkout_field_process()
	{
		// if the field is set, if not then show an error message.
		if (!$_POST['customised_field_name']) wc_add_notice(__('Please enter value.') , 'error');
	}

	function customise_checkout_field($checkout)
	{
        $card_type = 'skidata';
		$cart = STCart::get_carts();
		$number_adult = 0;
		$number_child = 0;
		$number_young = 0;
		$number_senior = 0;
		$number_baby = 0;
		if(!empty($cart)){
			$cart_temp = array_shift($cart);
            $card_type = trim(get_post_meta($cart_temp['data']['st_booking_id'], 'st_card_type', true));
			$number_adult = $cart_temp['data']['adult_number'];
			$number_child = $cart_temp['data']['child_number'];
			$number_young = $cart_temp['data']['infant_number'];
			$number_senior = $cart_temp['data']['senior_number'];
			$number_baby = $cart_temp['data']['baby_number'];
		}

		$arr_guest_fields = array(
			'adult' => array(
				'name' => __('Adult', ST_TEXTDOMAIN),
				'number' => $number_adult,
			),
			'child' => array(
				'name' => __('Children', ST_TEXTDOMAIN),
				'number' => $number_child,
			),
			'young' => array(
				'name' => __('Young', ST_TEXTDOMAIN),
				'number' => $number_young,
			),
			'senior' => array(
				'name' => __('Senior', ST_TEXTDOMAIN),
				'number' => $number_senior,
			),
			'baby' => array(
				'name' => __('Baby', ST_TEXTDOMAIN),
				'number' => $number_baby,
			),
		);

		?>
        <div class="id-option" style="width: 100%; margin-top: 30px">
            <h4>
                <?php
                if($card_type == 'skidata'){
                    echo __('Already have a skidata card?', ST_TEXTDOMAIN);
                }
                if($card_type == 'teamaxess'){
                    echo __('Already have a teamaxess card?', ST_TEXTDOMAIN);
                }
                if($card_type == 'voucher'){
                    echo __('Write the name of each skier', ST_TEXTDOMAIN);
                }
                ?>
            </h4>
            <div class="select-menu guest-check-out">
                <?php if($card_type == 'skidata' or $card_type == 'teamaxess'){ ?>
                    <div style="margin-left: 20px;">
                        <input style="float: left;" type="radio" id="select-option-yes" class="yes i-check check-has-guest" checked name="check_has_guest" value="yes">
                        <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-yes"><?php echo __('YES', ST_TEXTDOMAIN); ?></label>
                        <input style="float: left;" type="radio" id="select-option-no" class="no i-check check-has-guest" name="check_has_guest" value="no">
                        <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-no"><?php echo __('NO', ST_TEXTDOMAIN); ?></label><br/>
                    </div>
                <?php }else{
                    ?>
                    <div style="margin-left: 20px; display: none !important;">
                        <input style="float: left;" type="radio" id="select-option-yes" class="yes i-check check-has-guest" name="check_has_guest" value="yes">
                        <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-yes"><?php echo __('YES', ST_TEXTDOMAIN); ?></label>
                        <input style="float: left;" type="radio" id="select-option-no" class="no i-check check-has-guest" checked name="check_has_guest" value="no">
                        <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-no"><?php echo __('NO', ST_TEXTDOMAIN); ?></label><br/>
                    </div>
                    <?php
                } ?>
                <div class="menu-content1" style="width: 100%;    padding-top: 20px; display: block !important">

					<?php
					foreach ($arr_guest_fields as $k => $v) {
						?>
                        <div class="filed-<?php echo $k; ?>">
                            <div class="">
								<?php if (!empty($v['number'])) {
									echo '<p><b>' . $v['name'] . '</b></p>';
									for ($i = 0; $i < $v['number']; $i++) {
										?>
                                        <div class="mb10">
                                            <input style="height: 35px; width: 35%;" type="text" placeholder="<?php echo $v['name'] . ' ' . __('name', ST_TEXTDOMAIN) . ' ' . ($i + 1); ?>" name="st_guest[<?php echo $k; ?>][name][<?php echo $i; ?>]" autocomplete="off">
                                            <input style="height: 35px; width: 50%;" type="text" placeholder="<?php echo $v['name'] . ' ' . __('card number', ST_TEXTDOMAIN) . ' ' . ($i + 1); ?>"
                                                   name="st_guest[<?php echo $k; ?>][card][<?php echo $i; ?>]" class="name-card <?php echo ($card_type == 'voucher') ? 'hidden' : ''; ?>" autocomplete="off">
                                        </div>
										<?php
									}
									echo '<hr>';
								} ?>
                            </div>
                        </div>
						<?php
					}
					?>
                </div>
            </div>
        </div>

        <?php
		/*echo '<div id="customise_checkout_field"><h2>' . __('Heading') . '</h2>';
		woocommerce_form_field('customised_field_name', array(
			'type' => 'text',
			'class' => array(
				'my-field-class form-row-wide'
			) ,
			'label' => __('Customise Additional Field') ,
			'placeholder' => __('Guidence') ,
			'required' => true,
		) , $checkout->get_value('customised_field_name'));
		echo '</div>';*/
	}

	public function __getResortPriceAjax(){
	    $data = STInput::post('data');

	    $arr = [];
	    if(!empty($data)){
	        foreach ($data as $k => $v){
                $arr[$v['name']] = $v['value'];
            }
        }

		$type_tour = get_post_meta( $arr['item_id'], 'type_tour', true );
		$people_price[ 'total_price' ] = 0;

		$check_in = TravelHelper::convertDateFormat($arr['check_in']);
		$check_out = TravelHelper::convertDateFormat($arr['check_out']);

		if($arr['style'] == '1'){
		    $tour_origin = TravelHelper::post_origin($arr['item_id']);
            $people_price = STPrice::getPriceByPeopleTour( $tour_origin, strtotime( $check_in ), strtotime( $check_out ), $arr['adult_number'], $arr['child_number'], $arr['infant_number'], $arr['senior_number'], $arr['baby_number'] );

		}elseif($arr['style'] == '2'){
	        $people_price = STPrice::getPriceByPeopleTourPackDay( $arr['item_id'],  strtotime( $check_in ), strtotime( $check_out ), $arr['adult_number'], $arr['child_number'], $arr['infant_number'], $arr['senior_number'], $arr['baby_number'], $arr['pass_day'] );
        }

		$sale_price          = STPrice::getSaleTourSalePrice( $arr['item_id'], $people_price[ 'total_price' ], $type_tour, strtotime( $arr['check_in'] ) );


		echo json_encode(array(
		   'status' => 1,
		   'price' => TravelHelper::format_money($sale_price)
        ));die;
    }

    function _add_js_tmpl(){

        ?>

        <script type="text/html" id="tmpl-destination-marker">

            <?php

            $html = '<div class="marker" title="{{data.tax_name}}">

                            <input type="hidden" class="location-name oceaus-des-save" name="{{data.param_name}}[data_pos][{{data.index}}][tax]" value="{{data.tax}}">

                            <input type="hidden" class="location-top oceaus-des-save" name="{{data.param_name}}[data_pos][{{data.index}}][top]" value="">

                            <input type="hidden" class="location-left oceaus-des-save" name="{{data.param_name}}[data_pos][{{data.index}}][left]" value="">

                            <input type="hidden" class="location-size oceaus-des-save" name="{{data.param_name}}[data_pos][{{data.index}}][size]" value="{{data.size}}">

                            <a class="marker-close" href="#"><i class="fa fa-times"></i></a>

                        </div>';

            echo do_shortcode($html);

            ?>

        </script>

        <?php

    }

	public function __getResortByLocationID(){
		$location_id = STInput::post('location_id');

		$location_str = "_" . $location_id . "_";

		global $wpdb;
		$sql = $wpdb->prepare("SELECT post_id FROM {$wpdb->prefix}st_tours WHERE FIND_IN_SET(%s, multi_location) > 0", $location_str);

		$result = $wpdb->get_results($sql, ARRAY_A);

		$ids = [];

		$res = [];
		if(!empty($result)){
			foreach ($result as $k => $v){
				array_push($ids, $v['post_id']);
				$res[] = array(
					'id' => $v['post_id'],
					'name' => get_the_title($v['post_id'])
				);
			}
		}

		if(!empty($ids)){
			$str = implode(',', $ids);
			$sql1 = "SELECT ID, post_title FROM {$wpdb->prefix}posts WHERE ID NOT IN ({$str}) AND  post_status = 'publish' AND post_type = 'st_tours'";
		}else{
			$sql1 = "SELECT ID, post_title FROM {$wpdb->prefix}posts WHERE  post_status = 'publish' AND post_type = 'st_tours'";
		}

		$result1 = $wpdb->get_results($sql1, ARRAY_A);

		if(!empty($result1)){
			foreach ($result1 as $kk => $vv){
				$res[] = array(
					'id' => $vv['ID'],
					'name' => $vv['post_title']
				);
			}
		}

		echo json_encode($res);die;
	}
}
new STResort();