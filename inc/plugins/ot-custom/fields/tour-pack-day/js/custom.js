jQuery(function ($) {
    $(document).on("focus", ".st-datepicker", function(){
        var t= $(this);
        t.datepicker({
            dateFormat: t.data('date_format').replace('yy', '')
        }).on('changeDate', function (e) {
          console.log(e);
        });
    });

    var newItem = {
        adult: [],
        child: [],
        young: [],
        senior: [],
        baby: []
    }

    $(document).on('click', '#tour-package-save-hotel', function (e) {
        e.preventDefault();
        var t = $(this);

        /*Get data age*/
        var listTable = $('.stour-list-custom-hotel');
        var arr_table = {
            adult: [],
            child: [],
            young: [],
            senior: [],
            baby: []
        };
        listTable.each(function () {
            var tbl = $(this);
            var tbl_type = tbl.data('type');
            var j = 0;
            tbl.find('tbody tr').not('.parent-row').each(function () {
                var item_custom = $(this);
                arr_table[tbl.data('type')][j] = {
                    'pass_name': $('input.pass-name', item_custom).val(),
                    'pass_date': $('input.pass-date', item_custom).val(),
                    'pass_price': $('input.pass-price', item_custom).val(),
                };
                j++;
            });
        });

        /*End get data age*/

        var parentType = $('.stour-package');
        var boxList = $('#stour-list-hotel', parentType);
        boxList.find('.overlay').show();
        $('.form-message', parentType).html('');

        $.ajax({
            url: ajaxurl,
            dataType: 'json',
            type: 'post',
            data: {
                action: 'st_save_tour_pack_day',
                list_pack: $('#list-pack').val(),
                table_data: JSON.stringify(arr_table),
                post_id: t.data('post-id')
            },
            success: function (respond) {
                if (respond.status == false) {
                    $('.form-message', parentType).html('<div class="alert alert-error">' + respond.message + '</div>');
                } else {
                    $('.form-message', parentType).html('<div class="alert alert-success">' + respond.message + '</div>');
                }
                boxList.find('.overlay').hide();
            },
            error: function (e) {
                console.log('Can not get the availability slot. Lost connect with your sever');
            }
        });
    });


    $(document).on('click', '.btn-add-custom-package', function (e) {
        e.preventDefault();
        var t = $(this);
        var age = t.data('age');
        var parent = t.closest('.custom-hotel-data-item');
        var table = parent.find('table.stour-list-custom-hotel tbody');
        var tr = table.find("tr.parent-row").clone().removeClass('parent-row').show();
        tr.insertAfter(table.find('tr:last'));
    });
    $(document).on('click', '.hotel-del', function (e) {
        e.preventDefault();
        var t = $(this);
        t.closest('tr').remove();
    });

    /* Render list pack */
    $('#render-list-pack').click(function (e) {
        e.preventDefault();
       var listPack = $('#list-pack').val();
       listPack = listPack.split("|");
       listPackFilter = [];
       //Remove empty item and trim string
       for (var i = 0; i < listPack.length; i++){
           if(listPack[i].trim() != '') {
               listPackFilter[i] = listPack[i].trim();
           }
       }

        var listTable = $('.stour-list-custom-hotel');
        listTable.each(function () {
            var listPackFilterTemp = Object.assign([], listPackFilter);
            var t = $(this);
            var tdList = t.find('tbody tr').not('.parent-row');
            tdList.each(function () {
                var tc = $(this);
                var textVal = tc.find('.pass-name').val();
                if (listPackFilterTemp.includes(textVal.trim())) {
                    var index = listPackFilterTemp.indexOf(textVal.trim());
                    if (index !== -1) listPackFilterTemp.splice(index, 1);
                } else {
                    tc.remove();
                }
            });
            if (listPackFilterTemp.length > 0) {
                for (var ii = 0; ii < listPackFilterTemp.length; ii++) {
                    var tr = t.find("tr.parent-row").clone().removeClass('parent-row').show();
                    tr.find('.pass-name').val(listPackFilterTemp[ii]);
                    tr.insertAfter(t.find('tr:last'));
                }
            }
        });
    });
});