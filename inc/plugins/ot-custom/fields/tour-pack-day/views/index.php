<?php
global $post;
$post_id = $post->ID;
if (!empty($post_id)) {
    $list_pack_day = get_post_meta($post_id, 'st_list_pack_day', true);
    $tour_package_custom = get_post_meta($post_id, 'st_tour_pack_day_list', true);
    ?>
    <div class="stour-package">
        <div class="form-message"></div>

        <div class="form-group format-settings form-render-pass">
            <label><?php echo __('Create list Pass Name (Separated by the "|")', ST_TEXTDOMAIN); ?></label>
            <input id="list-pack" type="text" class="form-control widefat" value="<?php echo $list_pack_day; ?>"/>
            <button id="render-list-pack" class="button button-primary button-large btn-render-pass"><?php echo __('Render Pass Name', ST_TEXTDOMAIN); ?></button>
        </div><br />
        <div id="stour-list-hotel">
            <div class="overlay">
                <span class="spinner is-active"></span>
            </div>
            <!-- Tab -->
            <div class="ot-metabox-tabs">
                <ul class="ot-metabox-nav">
                    <li>
                        <a href="#tour-package-adult"><?php echo __('Adults', ST_TEXTDOMAIN); ?></a>
                    </li>
                    <li>
                        <a href="#tour-package-child"><?php echo __('Children', ST_TEXTDOMAIN); ?></a>
                    </li>
                    <li>
                        <a href="#tour-package-young"><?php echo __('Young', ST_TEXTDOMAIN); ?></a>
                    </li>
                    <li>
                        <a href="#tour-package-senior"><?php echo __('Seniors', ST_TEXTDOMAIN); ?></a>
                    </li>
                    <li>
                        <a href="#tour-package-baby"><?php echo __('Babys', ST_TEXTDOMAIN); ?></a>
                    </li>
                </ul>
                <div class="stour-package-tab-content">
                    <div id="tour-package-adult" class="tab-content  stour-tab-content">
                        <div class="list-custom-hotel">
                            <?php echo st()->load_template('tours/elements/stour', 'package-custom-age', array('post_id' => $post_id, 'age' => 'adult', 'pack' => isset($tour_package_custom['adult']) ? $tour_package_custom['adult'] : [])); ?>
                        </div>
                    </div>
                    <div id="tour-package-child" class="tab-content  stour-tab-content">
                        <div class="list-custom-hotel">
			                <?php echo st()->load_template('tours/elements/stour', 'package-custom-age', array('post_id' => $post_id, 'age' => 'child', 'pack' => isset($tour_package_custom['child']) ? $tour_package_custom['child'] : [])); ?>
                        </div>
                    </div>
                    <div id="tour-package-young" class="tab-content  stour-tab-content">
                        <div class="list-custom-hotel">
			                <?php echo st()->load_template('tours/elements/stour', 'package-custom-age', array('post_id' => $post_id, 'age' => 'young', 'pack' => isset($tour_package_custom['young']) ? $tour_package_custom['young'] : [])); ?>
                        </div>
                    </div>
                    <div id="tour-package-senior" class="tab-content  stour-tab-content">
                        <div class="list-custom-hotel">
			                <?php echo st()->load_template('tours/elements/stour', 'package-custom-age', array('post_id' => $post_id, 'age' => 'senior', 'pack' => isset($tour_package_custom['senior']) ? $tour_package_custom['senior'] : [])); ?>
                        </div>
                    </div>
                    <div id="tour-package-baby" class="tab-content  stour-tab-content">
                        <div class="list-custom-hotel">
			                <?php echo st()->load_template('tours/elements/stour', 'package-custom-age', array('post_id' => $post_id, 'age' => 'baby', 'pack' => isset($tour_package_custom['baby']) ? $tour_package_custom['baby'] : [])); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Tab -->
            <input type="submit" id="tour-package-save-hotel"
                   class="option-tree-ui-button button button-primary stour-package-button" name="tour-package-save-hotel"
                   data-post-id="<?php echo $post_id; ?>"
                   value="<?php echo __('Save data', ST_TEXTDOMAIN); ?>">
        </div>
    </div>
    <?php
} else {
    echo __('The function not exists!', ST_TEXTDOMAIN);
}
?>
