<?php
    if ( !class_exists( 'ST_Tour_Pack_Day_Field' ) ) {
        class ST_Tour_Pack_Day_Field
        {
            public $url;
            public $dir;

            function __construct()
            {
	            $this->dir = get_stylesheet_directory() . '/inc/plugins/ot-custom/fields/tour-pack-day';
	            $this->url = get_stylesheet_directory_uri() . '/inc/plugins/ot-custom/fields/tour-pack-day';


                add_action( 'admin_enqueue_scripts', array( $this, 'add_scripts' ) );
                //add_action('save_post', [$this, '_save_post'], 10, 2);
            }

            function init()
            {

                if ( !class_exists( 'OT_Loader' ) ) return false;


                add_filter( 'ot_option_types_array', array( $this, 'ot_add_custom_option_types' ) );

            }

            function add_scripts()
            {
                wp_register_script( 'tour-package-day-js', $this->url . '/js/custom.js', array( 'jquery' ), NULL, TRUE );
                wp_register_style( 'tour-package-day-css', $this->url . '/css/custom.css');
            }

            function ot_add_custom_option_types( $types )
            {
                $types[ 'st_tour_pack_day' ] = __( 'Tour Pack Day', ST_TEXTDOMAIN );

                return $types;
            }

            function load_view( $view = false, $data = array() )
            {

                if ( !$view ) $view = 'index';

                $file_name = $this->dir . '/views/' . $view . '.php';

                if ( file_exists( $file_name ) ) {
                    extract( $data );

                    ob_start();

                    include $file_name;

                    return @ob_get_clean();
                }
            }
        }

        $tour_package_day = new ST_Tour_Pack_Day_Field();
	    $tour_package_day->init();

        if ( !function_exists( 'ot_type_st_tour_pack_day' ) ) {
            function ot_type_st_tour_pack_day( $args = array() )
            {
                $tour_package_day = new ST_Tour_Pack_Day_Field();
                $default = array(
                    'field_name' => ''
                );
                $args = wp_parse_args($args, $default);

                wp_enqueue_script( 'tour-package-day-js' );
                wp_enqueue_style( 'tour-package-day-css' );

                echo $tour_package_day->load_view(false, $args);
            }
        }
    }