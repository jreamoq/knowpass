jQuery(document).ready(function ($) {
    if($('#wp_is_mobile').length <= 0) {
        var $title_menu = $('ul.slimmenu').data('title');
        if ($('ul.slimmenu').length) {
            $('ul.slimmenu').slimmenu({
                resizeWidth: '5000',
                collapserTitle: $title_menu,
                animSpeed: 250,
                indentChildren: true,
                childrenIndenter: '',
                expandIcon: "<i class='fa fa-angle-down'></i>",
                collapseIcon: "<i class='fa fa-angle-up'></i>",
            });
        }
    }

    $('.guest-check-out #select-option-no').on('ifChanged', function () {
        $('.guest-check-out .name-card').fadeOut();
    });
    $('.guest-check-out #select-option-yes').on('ifChanged', function () {
        $('.guest-check-out .name-card').fadeIn();
    });
    $('#menu1 .collapse-button').click(function () {
       $('#main-header .main_menu_wrap#menu1 .slimmenu').toggleClass('active');
    });

    $(document).on('click', '.option-wrapper .option', function(event) {
        var t = $(this);
        var valueChoose = t.data('value');
        $('#field-tour-item_id option').remove();
        $('#field-tour-item_id').append('<option value="">'+ ct_checkout_text.text_loading +'</option>');
        $('.main-search button').attr('type', 'button');
        $.ajax({
            url: st_params.ajax_url,
            dataType: 'json',
            type: 'post',
            data: {
                action: 'st_get_resort_by_location',
                location_id: valueChoose,
            },
            success: function (res) {
                $('#field-tour-item_id option').remove();
                if(res.length){
                    var te = "<option value=''>---"+ ct_checkout_text.text_select +"---</option>";
                    for(var i = 0; i < res.length; i++){
                        te += "<option value='"+ res[i]['id'] +"'>"+ res[i]['name'] +"</option>";
                    }
                }else{
                    var te = "<option value=''>---"+ ct_checkout_text.text_no_search_result +"---</option>";
                }
                $('#field-tour-item_id').append(te);
                $('.main-search button').attr('type', 'submit');
            },
            error: function (e) {
                $('.main-search button').attr('type', 'submit');
                $('#field-tour-item_id option').remove();
                alert('Can not get the resort by location');
            }
        });
    });

    /* Search click */
    $('.topbar .top-bar-style-7 .btn-topbar').click(function () {

        var form_search = $(this).closest('.top-form-search');

        if(form_search.find('.content-form-search').hasClass('active')){

            form_search.find('.content-form-search').removeClass('active');

        }else{

            form_search.find('.content-form-search').addClass('active');

        }

    });

    $('.topbar .top-bar-style-7 .nav-icon-bar').click(function () {

        var content_menu = $(this).closest('.option-mid');

        if(content_menu.find('.content-menu').hasClass('active')){

            content_menu.find('.content-menu').removeClass('active');

            content_menu.find('.content-menu').slideUp();

        }else{

            content_menu.find('.content-menu').addClass('active');

            content_menu.find('.content-menu').slideDown();

        }

    });

    $('.topbar .btn-close-menu').click(function () {

        $(this).closest('.content-menu').removeClass('active');

    });

    $('.oceaus_dl_desktop_menu.style-left').each(function(){

        $( this ).dlmenu({

            animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }

        });

    });

    if( $('.oceaus_dl_desktop_menu').length ){

        $('.oceaus_dl_desktop_menu').niceScroll();

    }

    $('.oceaus_dl_mobile_menu').each(function(){

        var $fotter = $(this).find('.html-footer').html();

        $(this).find('.dl-menu').append('<li class="content-footer">'+$fotter+'</li>');

        $( this ).dlmenu({});

    });

    if($('.choice-pass-name').length){

    }
});