<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 21/08/2015
 * Time: 9:45 SA
 */

add_action('wp_enqueue_scripts','ct_add_scripts');
add_action('admin_enqueue_scripts','ct_admin_add_scripts');
function ct_add_scripts(){
	wp_enqueue_style( 'ct-single-tour-css', get_stylesheet_directory_uri() . '/css/custom.css' );
	wp_enqueue_style( 'ct-topbar-css', get_stylesheet_directory_uri() . '/css/topbar.css' );

    wp_enqueue_script( 'st-custom-js', get_stylesheet_directory_uri() . '/js/custom.js', [ 'jquery' ], NULL, true );

	wp_register_script( 'ct-bulk-calendar', get_stylesheet_directory_uri() . '/js/bulk-calendar.js', [ 'jquery', ], null, true );
	if ( is_singular( 'st_tours' ) ) {
		wp_enqueue_script( 'st-single-tour-js', get_stylesheet_directory_uri() . '/js/single-tour.js', [ 'jquery' ], NULL, true );
	}

	if ( get_query_var( 'sc' ) == 'edit-tours' ) {
		wp_enqueue_script( 'ct_tour_availability_partner', get_stylesheet_directory_uri() . '/js/availability_tour_partner.js', [ 'jquery' ], NULL, TRUE );
	}

	wp_enqueue_style('component',  get_stylesheet_directory_uri() . '/js/mutimenu/css/component.css');

	wp_enqueue_script('js-modernizr', get_stylesheet_directory_uri() . '/js/mutimenu/js/modernizr.custom.js', array('jquery'), null, true);
	wp_enqueue_script('js-dlmenu', get_stylesheet_directory_uri() . '/js/mutimenu/js/jquery.dlmenu.js', array('jquery'), null, true);


	//if(is_singular('st_tours') or get_query_var( 'sc' ) == 'edit-tours') {
		wp_localize_script( 'jquery', 'ct_checkout_text', [
			'text_young'       => __( "Young", ST_TEXTDOMAIN ),
			'text_senior'      => __( "Senior", ST_TEXTDOMAIN ),
			'text_baby'       => __( "Baby", ST_TEXTDOMAIN ),
            'text_loading' => __('Loading...', ST_TEXTDOMAIN),
            'text_select' => __('Select', ST_TEXTDOMAIN),
            'text_no_search_result' => __('No search results', ST_TEXTDOMAIN)
		] );
	//}
}
function ct_admin_add_scripts(){
    wp_enqueue_style( 'ct-admin', get_stylesheet_directory_uri() . '/css/admin.css' );
	wp_register_script( 'ct-bulk-calendar', get_stylesheet_directory_uri() . '/js/bulk-calendar.js', [ 'jquery', ], null, true );
    wp_localize_script( 'jquery', 'ct_checkout_text', [
        'text_young'       => __( "Young", ST_TEXTDOMAIN ),
        'text_senior'      => __( "Senior", ST_TEXTDOMAIN ),
        'text_baby'       => __( "Baby", ST_TEXTDOMAIN ),
    ] );
}
function wpdocs_dequeue_script() {
	if(is_singular('st_tours')) {
		wp_dequeue_script( 'single-tour-js' );
	}
	wp_dequeue_script( 'tour_availability_partner' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

if (!function_exists('st_vc_header')){
	function st_vc_header($attr){
		if(get_post_type(get_the_ID()) == 'st_tours'){
			$price_html = STTour::get_price_html(get_the_ID());
		}else{
			$price_html = STActivity::get_price_html(get_the_ID());
		}
		$return = '
        <header class="booking-item-header">
            <div class="row">
                <div class="col-md-9 col-xs-12">'.
		          st()->load_template('tours/elements/header',null,array('attr'=>$attr))
		          .'</div>
                <div class="col-md-3 col-xs-12 text-right price_activity">
						<p class="booking-item-header-price">' .
		          $price_html
		          .'</p>
                </div>
            </div>
        </header>';
		return $return ;
	}
	st_reg_shortcode('st_header','st_vc_header');
}

/* Tour info shortcode */
if(!function_exists( 'st_info_tours_func' )) {
    function st_info_tours_func($attr)
    {
        if(is_singular( 'st_tours' )) {
            $default = array(
                'style'      => '2' ,
                'font_size' => 3,
                'title1'    => __("Tour Informations" , ST_TEXTDOMAIN),
                'title2'    => __("Place Order" , ST_TEXTDOMAIN)
            );
            $dump = wp_parse_args( $attr , $default );
            extract( $dump );
            if ($style ==1) $style ="";
            return st()->load_template( 'tours/elements/info-tours' , '2', array('attr'=> $attr));
        }
    }

    st_reg_shortcode( 'st_info_tours' , 'st_info_tours_func' );
}

if(!function_exists('oceaus_vc_destinations')){
    function oceaus_vc_destinations($atts, $content = false){
        $html = $style = $list_destinations = $title = $sub_title = $description = $button_name = $button_link = $link = $list_location = '';
        extract(shortcode_atts(array(
            'style' => 'style-1',
            'list_destinations' => '',
            'title' => '',
            'sub_title' => '',
            'description' => '',
            'button_name' => esc_html__('CHECK ALL DESTINATIONS','oceaus'),
            'button_link' => '',
            'list_location' => ''
        ),$atts));

        if(!empty($button_link)){
            $link = vc_build_link($button_link);
        }

        parse_str(urldecode($list_destinations), $data_destinations);

        $html .= '<div class="oceaus-destination '.$style.'">';

        $old_style = $style;

        if($style == 'style-3'){
            $style ='style-1';
        }

        $html .= st()->load_template('selements/st-destinations/'.$style,false, array(
            'style' => $style,
            'old_style' =>$old_style,
            'list_destinations' => $data_destinations,
            'list_location' => $list_location,
            'description' => $description,
            'title' => $title,
            'sub_title' => $sub_title,
            'button_name' => $button_name,
            'button_link' => $link
        ));
        $html .= '</div>';

        return $html;
    }
    st_reg_shortcode('oceaus_destinations', 'oceaus_vc_destinations');
}

function dd($arr){
	/*echo '<pre>';
	print_r($arr);
	echo '</pre>';*/
}