<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Header
 *
 * Created by ShineTheme
 *
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2, minimum-scale=1">
    <link rel="manifest" href="<?php echo get_template_directory_uri() ?>/manifest.json"/>
    <meta name="theme-color" content="#ED8323"/>
    <meta name="robots" content="follow"/>
	<?php if ( defined( 'ST_TRAVELER_VERSION' ) ) { ?>
        <meta name="traveler" content="<?php echo esc_attr( ST_TRAVELER_VERSION ) ?>"/>  <?php }; ?>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if ( ! function_exists( '_wp_render_title_tag' ) ): ?>
        <title><?php wp_title( '|', true, 'right' ) ?></title>
	<?php endif; ?>
	<?php wp_head(); ?>
    <script>
        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        window.fbAsyncInit = function () {

            FB.init({
                appId: st_params.facbook_app_id,
                cookie: true,  // enable cookies to allow the server to access
                               // the session
                xfbml: true,  // parse social plugins on this page
                version: 'v3.1' // use graph api version 2.8
            });

        };
    </script>
</head>
<?php
$menu_style = st()->get_option( 'menu_style', "1" );
if ( $menu_style == '3' ) {
	$bclass = 'body-header-3';
} else {
	$bclass = 'menu-style-' . $menu_style;
}
?>
<body <?php body_class( $bclass ); ?>>
<?php do_action( 'before_body_content' ) ?>
<?php $enable_popup_login = st()->get_option( 'enable_popup_login', 'off' );
if ( $enable_popup_login == 'on' ) {
	?>
	<?php echo st()->load_template( 'login/popup-login', null, array() ); ?>
	<?php echo st()->load_template( 'login/popup-register', null, array() ); ?>
<?php } ?>
<?php
$enable_preload = st()->get_option( 'search_enable_preload', 'on' );
if ( $enable_preload == 'on' && ! TravelHelper::is_service_search() ) {
	echo st()->load_template( 'search-loading' );
}
?>
<?php
if ( wp_is_mobile() ) {
	echo '<div id="wp_is_mobile"></div>';
}
?>
<div id="st_header_wrap" class="global-wrap header-wrap <?php echo apply_filters( 'st_container', true ) ?>">
    <div class="row" id="st_header_wrap_inner">
		<?php
		$is_topbar = st()->get_option( 'enable_topbar', 'off' );
		if ( $is_topbar == "on" ) {
			//echo st()->load_template('menu/top_bar' , null,  array()) ;
		}
		?>
		<?php
		$menu_style = st()->get_option( 'menu_style', "1" );
		//echo st()->load_template('menu/style' , $menu_style ,  array()) ;
		?>
        <!--New menu-->
        <div class="topbar">
            <div class="container">
            <div class="topbar-content top-bar-style-7" data-offset="0">
                <div class="background-scroll st_1538676208"></div>
                <div class="control-left">
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="logo">
								<?php $logo_url = st()->get_option( 'logo' ); ?>
                                <a href="<?php echo home_url( '/' ) ?>">
									<?php
									if ( ! empty( $logo_url ) ) {
										?>
                                        <img src="<?php echo esc_url( $logo_url ); ?>" alt="logo"
                                             title="<?php bloginfo( 'name' ) ?>">
										<?php
									}
									?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-right">
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="oceaus-menu-position-right">
                                <div class="nav-icon-bar btn-control-menu">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </div>
                                <div class="content-menu <?php  if ( !has_nav_menu('primary') ) echo 'hide' ?>">
                                    <div class="menu-header">
                                        <button class="btn btn-close btn-close-menu"><?php esc_html_e("Close","oceaus") ?></button>
                                    </div>
                                    <div class="dl-content-menu">
                                        <div class="dl-menuwrapper oceaus_dl_desktop_menu style-left">
				                            <?php
				                            if ( has_nav_menu('primary') ) {
					                            $args = array(
						                            'theme_location' => 'primary',
						                            'menu_class'      => 'dl-menu dl-menuopen',
						                            'container'      => '',
						                            'walker'          => new Oceaus_Dl_Menu_Walker,
					                            );
					                            wp_nav_menu($args);
				                            }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="option-item">
                        <div class="option-mid">
							<?php if ( ! is_user_logged_in() ) { ?>
                                <div class="my-account no_login">
                                    <div class="dropdown">
            <span class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </span>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <form action=""
                                                      method="post" id="wpbooking-login-form"
                                                      class="login-register-form st_login_form_popup">
                                                    <input type="hidden" name="action" value="wpbooking_do_login">
                                                    <input type="hidden" name="url" value="">
                                                    <p class="title"><?php echo __('Login', ST_TEXTDOMAIN); ?></p>
                                                    <div class="form-group left-addon">
                                                        <i class="fa fa-user-circle"></i>
                                                        <input type="text" class="form-control"
                                                               placeholder="<?php echo __('User Name or Email', ST_TEXTDOMAIN); ?>" required="" value=""
                                                               name="login" id="pop-login_name">
                                                    </div>
                                                    <div class="form-group left-addon">
                                                        <i class="fa fa-key" aria-hidden="true"></i>
                                                        <input type="password" class="form-control" required=""
                                                               placeholder="<?php echo __('Password', ST_TEXTDOMAIN); ?>" id="pop-login_password" name="password">
                                                    </div>
                                                    <p class="forgot">
                                                        <?php
                                                        $url_reset = st()->get_option('page_reset_password', '');
                                                        if( $url_reset ){
                                                            $url_reset = get_permalink( $url_reset );
                                                        }else{
                                                            $url_reset = wp_lostpassword_url();
                                                        }
                                                        ?>
                                                        <a href="<?php echo esc_url($url_reset); ?>"
                                                           class="lost-password"><?php echo __('Forgot password?', ST_TEXTDOMAIN); ?></a>
                                                    </p>
                                                    <div class="btn-submit-form">
                                                        <button class="btn btn-primary btn-login-popup" name="login_submit" type="submit">
                                                            <?php echo __('Sign In', ST_TEXTDOMAIN); ?>
                                                            <img alt="loading" src="<?php echo get_template_directory_uri(); ?>/img/ajax-login.gif" style="display: none;">
                                                        </button>
                                                    </div>
                                                    <div class="notice_login"></div>
                                                </form>

                                            </li>
                                            <li>
                                                <p class="register-text">
                                                    <?php echo __("Don't have an account?", ST_TEXTDOMAIN); ?> <br>
                                                    <span class="text-color"><?php echo __('Sign up now!', ST_TEXTDOMAIN); ?></span>
                                                </p>
                                                <?php
                                                $page_user_register = st()->get_option('page_user_register');
                                                $page_user_register = esc_url(get_the_permalink($page_user_register));
                                                ?>
                                                <a href="<?php echo $page_user_register; ?>"
                                                   class="btn btn-dark"><?php echo __('Sing up for free', ST_TEXTDOMAIN); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
							<?php } else {
								$account_dashboard = st()->get_option( 'page_my_account_dashboard' );
								$location          = '#';
								if ( ! empty( $account_dashboard ) ) {
									$location = get_permalink( $account_dashboard );
								}
								?>
                                <div class="nav-drop nav-symbol is_login">
                                    <div class="dropdown">
                                <span class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <?php
                $current_user = wp_get_current_user();
                echo st_get_profile_avatar( $current_user->ID, 40 );
                //echo st_get_language('hi').', '.$current_user->display_name;
                printf( __( 'hi, %s', ST_TEXTDOMAIN ), $current_user->display_name );
                ?>
            </span>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="<?php echo esc_url( $location ) ?>"><?php echo __( 'User dashboard', ST_TEXTDOMAIN ); ?></a>
                                            </li>
                                            <li><a class="btn-st-logout"
                                                   href="<?php echo wp_logout_url( home_url() ) ?>"><?php st_the_language( 'sign_out' ) ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
							<?php } ?>
                        </div>
                    </div>
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="top-form-search">
                                <strong class="btn-topbar"><i class="fa fa-search"></i></strong>
                                <div class="content-form-search">
                                    <form class="main-header-search" action="<?php echo home_url( '/' ); ?>"
                                          method="get">
                                        <input name="post_type" value="post" type="hidden">
                                        <input type="text"
                                               placeholder="<?php echo __( 'Search information here', ST_TEXTDOMAIN ); ?>"
                                               name="s" value="<?php echo get_search_query() ?>"
                                               class="form-control st-top-ajax-search">
                                        <button type="submit" class="fa fa-search btn hide"></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="option-item pl0">
                        <div class="option-mid">
                            <div class="dropdown">
								<?php echo st()->load_template( "menu/shopping_cart", null, array(
									'container' => "span",
									"class"     => "top-user-area-shopping"
								) ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="dropdown">
								<?php echo st()->load_template( "menu/currency_select", null, array(
									'container' => "span",
									"class"     => "nav-drop nav-symbol"
								) ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="dropdown lang">
                                <?php
                                echo st()->load_template("menu/language_select", null, array('container' => "span", "class" => "top-user-area-lang"));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- Menu mobile -->
        <div class="header-mobile">
        <div class="style-light">
            <div class="oceaus-navbar-header">
                <div class="control-left">
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="logo">
                                <?php $logo_url = st()->get_option( 'logo' ); ?>
                                <a href="<?php echo home_url( '/' ) ?>">
                                    <?php
                                    if ( ! empty( $logo_url ) ) {
                                        ?>
                                        <img src="<?php echo esc_url( $logo_url ); ?>" alt="logo"
                                             title="<?php bloginfo( 'name' ) ?>">
                                        <?php
                                    }
                                    ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-right">
                    <div class="option-item">
                        <div class="option-mid">
                            <div class="dl-menuwrapper oceaus_dl_mobile_menu">
                                <div class="nav-icon-bar dl-trigger">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </div>
                                <?php
                                if ( has_nav_menu('primary') ) {
                                    $args = array(
                                        'theme_location' => 'primary',
                                        'menu_class'      => 'dl-menu',
                                        'container'      => '',
                                        'walker'          => new Oceaus_Dl_Menu_Walker,
                                    );
                                    wp_nav_menu($args);
                                }?>
                                <div class="html-footer hide">
                                    <div class="navbar-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!--End New menu-->
    </div>
</div>
<div class="global-wrap <?php echo apply_filters( 'st_container', true ) ?>">
    <div class="row">
