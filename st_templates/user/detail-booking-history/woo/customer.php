<div class="info">
    <table cellpadding="0" cellspacing="0" width="100%" border="0px" class="tb_cart_customer">
        <tbody>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('First name ' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_first_name', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('Last name ' , ST_TEXTDOMAIN) ; ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_last_name', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('Email ' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_email', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('Phone ' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_phone', true) ?>
            </td>
        </tr>

        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('Address Line 1' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_address_1', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('Address Line 2' , ST_TEXTDOMAIN ) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_address_2', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('City' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_city', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('State/Province/Region' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_state', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('ZIP code/Postal code' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_postcode', true) ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <strong><?php echo __('Country' , ST_TEXTDOMAIN) ;  ?></strong></td>
            <td align="right" class="text-right" style="border-bottom: 1px dashed #ccc;padding:10px;">
                <?php echo get_post_meta($order_id, '_billing_country', true) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="" class=""
                style="border-bottom: 1px dashed #ccc;padding:10px;vertical-align: top">
		        <?php
		        $has_guest_card = get_post_meta($order_id, 'has_guest_card', true);
		        $guest_card = get_post_meta($order_id, 'guest_card', true);
		        if(!empty($guest_card)){
			        foreach ($guest_card as $k => $v){
				        if($k == 'young')
					        $number = get_post_meta($order_id, 'stw_infant_number', true);
				        else
					        $number = get_post_meta($order_id, 'stw_' . $k . '_number', true);
				        if($number > 0){
					        $guest_type = '';
					        switch ($k){
						        case 'adult':
							        $guest_type = __('Adult', ST_TEXTDOMAIN);
							        break;
						        case 'child':
							        $guest_type = __('Children', ST_TEXTDOMAIN);
							        break;
						        case 'young':
							        $guest_type = __('Young', ST_TEXTDOMAIN);
							        break;
						        case 'senior':
							        $guest_type = __('Senior', ST_TEXTDOMAIN);
							        break;
						        case 'baby':
							        $guest_type = __('Baby', ST_TEXTDOMAIN);
							        break;
					        }
					        echo '<b>'. $guest_type .'</b><br />';
					        if(!empty($v)){
						        $styleinner='40%';
						        if($has_guest_card != 'yes'){
							        $styleinner='100%';
						        }
						        ?>
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td style="width: <?php echo $styleinner; ?>"><b><?php echo __('Name', ST_TEXTDOMAIN); ?></b></td>
								        <?php if($has_guest_card == 'yes'){ ?>
                                            <td style=""><b><?php echo __('Card number', ST_TEXTDOMAIN); ?></b></td>
								        <?php } ?>
                                    </tr>
							        <?php
							        foreach ($v as $kk => $vv){
								        if(!empty($vv['name']) or !empty($vv['card'])) {
									        ?>
                                            <tr>
                                                <td style="width: <?php echo $styleinner; ?>"><?php echo $vv['name']; ?></td>
										        <?php if ($has_guest_card == 'yes') { ?>
                                                    <td><?php echo $vv['card']; ?></td>
										        <?php } ?>
                                            </tr>
									        <?php
								        }
							        }
							        ?>
                                </table>
						        <?php
					        }
				        }
			        }
		        }
		        ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>