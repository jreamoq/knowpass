<?php
    /**
     * @package WordPress
     * @subpackage Traveler
     * @since 1.0
     *
     * Tours loop content 2
     *
     * Created by ShineTheme
     *
     */
    $col = 12 / 3;
    $info_price = STTour::get_info_price();
    global $post;
    $post_id = $post->ID;
    $st_show_number_avai = st()->get_option('st_show_number_avai', 'off');

    $dataWishList = STUser_f::get_icon_wishlist();

$url=st_get_link_with_search(get_permalink(),array('start','end','duration','people'),$_GET);
if(empty($taxonomy)) $taxonomy=false;
?>
<div class="col-md-6 col-sm-6 col-xs-12 style_box has-matchHeight " itemscope itemtype="http://schema.org/TouristAttraction">
    <?php echo STFeatured::get_featured(); ?>
    <div class="thumb custom-tour-grid-3">
        <?php if(!empty( $info_price['discount'] ) and $info_price['discount']>0 and $info_price['price_new'] >0) { ?>
            <?php echo STFeatured::get_sale($info_price['discount']); ?>
        <?php } ?>
        <header class="thumb-header">
            <a href="<?php echo esc_url($url) ?>" class="hover-img">
                <?php
                    $img = get_the_post_thumbnail( $post_id , array(800,600,'bfi_thumb'=>true), array('alt' => TravelHelper::get_alt_image(get_post_thumbnail_id( $post_id )))) ;
                    if(!empty($img)){
                        echo balanceTags($img);
                    }else{
                        echo '<img width="800" height="600" alt="no-image" class="wp-post-image" src="'.bfi_thumb(ST_TRAVELER_URI.'/img/no-image.png',array('width'=>800,'height'=>600)) .'">';
                    }
                ?>
               
	            <?php if(is_user_logged_in()){ ?>
                <a class="add-item-to-wishlist" data-id="<?php echo get_the_ID(); ?>" data-post_type="<?php echo get_post_type(get_the_ID()); ?>" rel="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo balanceTags($dataWishList['original-title']) ?>">
		            <?php echo balanceTags($dataWishList['icon']); ?>
                    <i class="fa fa-spinner loading""></i>
                </a>
                <?php } ?>
            </a>
            <?php echo st_get_avatar_in_list_service($post_id,35)?>
        </header>
        <div class="thumb-caption">
            
            <h5 class="thumb-title" style="font-weight: bold;text-align: center;padding-bottom: 20px; padding-top: 20px;">
                <a href="<?php echo esc_url($url)?>" class="text-darken">
                    <?php the_title(); ?>
                </a>
            </h5>
            <p class="booking-item-description"style="text-align: center;    padding-bottom: 15px;">
                        <?php echo st_get_the_excerpt_max_charlength(230); ?>
            </p>
            <?php if($address = get_post_meta($post_id,'address',true)) {?>
            <p class="mb0" style="text-align: center; padding-bottom: 15px;">
                <small><i class="fa fa-map-marker"></i> 
                    <?php
                        if(!empty($address)){
                            echo esc_html($address);
                        }
                    ?>
                </small>
            </p>
            <?php } ?>
            
            
             <div class="booking-item-rating" style="text-align: center; padding-bottom: 15px;width: 100%;">
                <ul class="icon-group booking-item-rating-stars">
                    <?php
                        $avg = STReview::get_avg_rate();
                        echo TravelHelper::rate_to_string($avg);
                    ?>
                </ul>
                <?php if(!wp_is_mobile()){ ?>
                                <span
                                    class="booking-item-rating-number"></span>
                <small style="font-size: 100%;">
                    <?php 
                        $commentinfo = wp_count_comments(get_the_ID());
                        $num_comments = ($commentinfo) ? (int)$commentinfo->approved : 0;  
                        if ( $num_comments == 0 ) {
                            $comments = __('No review', ST_TEXTDOMAIN);
                        } elseif ( $num_comments > 1 ) {
                            $comments = $num_comments . __(' reviews', ST_TEXTDOMAIN);
                        } else {
                            $comments = __('1 review', ST_TEXTDOMAIN);
                        }
                        echo $comments;
                    ?>
                </small>
                <?php } ?>
                <?php if(is_user_logged_in()){ ?>
                
            <?php } ?>
            </div>
           
            <p class="mb0 text-darken" style="text-align: center; padding-bottom: 15px;">
                <small class="price-section" stype="font-size: 100% !important;"> From <?php 

                echo STTour::get_price_html($post_id) ?> /1 day</small>
            </p>

	        <?php if(!empty(STInput::get('start')) && !empty(STInput::get('end')) && $st_show_number_avai == 'on'){ ?>
		        <?php echo st()->load_template('tours/elements/seat-availability', null, array()); ?>
	        <?php } ?>
             <a href="<?php echo esc_url($url)?>" style="text-align: center; display: block;">
                <span class="btn btn-primary btn_book" style="background: #a3a3a3; border-color: #a3a3a3;border-radius: 30px;    padding: 10px 35px;margin-bottom: 20px;"><?php st_the_language('tour_book_now') ?></span>
            </a>
        </div>
    </div>
</div>

