<?php
    /**
     * @package WordPress
     * @subpackage Traveler
     * @since 1.0
     *
     * Tours loop content 1
     *
     * Created by ShineTheme
     *
     */
    $tours = new STTour();
    $info_price = STTour::get_info_price();
    $dataWishList = STUser_f::get_icon_wishlist();
    $st_show_number_avai = st()->get_option('st_show_number_avai', 'off');

$url=st_get_link_with_search(get_permalink(),array('start','end','duration','people'),$_GET);
if(empty($taxonomy)) $taxonomy=false;
?>
<li <?php post_class('booking-item') ?> itemscope itemtype="http://schema.org/TouristAttraction">
    <?php echo STFeatured::get_featured(); ?>
    <div class="row booking-row">
        <div class="col-md-4">
            <div class="booking-item-img-wrap">
                <a class="" href="<?php echo esc_url($url)?>">
                <?php 
                if (has_post_thumbnail()){
                the_post_thumbnail(array(600, 450, 'bfi_thumb' => true), array('alt' => TravelHelper::get_alt_image(get_post_thumbnail_id( ))));
                }
                else{
                    echo st_get_default_image();
                }
                 ?>
                </a>
                <?php echo st_get_avatar_in_list_service(get_the_ID(),35)?>
                <?php if(is_user_logged_in()){ ?>
                <a class="add-item-to-wishlist pos2" data-id="<?php echo get_the_ID(); ?>" data-post_type="<?php echo get_post_type(get_the_ID()); ?>" rel="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo balanceTags($dataWishList['original-title']) ?>">
                    <?php echo balanceTags($dataWishList['icon']); ?>
                    <i class="fa fa-spinner loading""></i>
                </a>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5 booking-minfo" style="border-right: 1px solid #00000029;">
            
            <a class="" href="<?php echo esc_url($url)?>">
                <h5 class="booking-item-title"><?php the_title() ?></h5>
            </a>
            <p class="booking-item-description">
                        <?php echo st_get_the_excerpt_max_charlength(230); ?>
            </p>
            <div class="booking-item-rating" style="width: 100%;">
                <ul class="icon-group booking-item-rating-stars">
                    <?php
                        $avg = STReview::get_avg_rate();
                        echo TravelHelper::rate_to_string($avg);
                    ?>
                </ul>
                <?php if(!wp_is_mobile()){ ?>
                                <span
                                    class="booking-item-rating-number"></span>
                <small style="font-size: 100%;">
                    <?php 
                        $commentinfo = wp_count_comments(get_the_ID());
                        $num_comments = ($commentinfo) ? (int)$commentinfo->approved : 0;  
                        if ( $num_comments == 0 ) {
                            $comments = __('No review', ST_TEXTDOMAIN);
                        } elseif ( $num_comments > 1 ) {
                            $comments = $num_comments . __(' reviews', ST_TEXTDOMAIN);
                        } else {
                            $comments = __('1 review', ST_TEXTDOMAIN);
                        }
                        echo $comments;
                    ?>
                </small>
                <?php } ?>
                <?php if(is_user_logged_in()){ ?>
                <a class="add-item-to-wishlist" style="padding: 5px 30px;float: right;" data-id="<?php echo get_the_ID(); ?>" data-post_type="<?php echo get_post_type(get_the_ID()); ?>" rel="tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo balanceTags($dataWishList['original-title']) ?>">
                <?php echo balanceTags($dataWishList['icon']); ?>
                <i class="fa fa-spinner loading""></i>
            </a>
            <?php } ?>
            </div>
            <?php if ($address = get_post_meta(get_the_ID(), 'address', true)): ?>
                <p class="booking-item-address"><i class="fa fa-map-marker"></i> <?php echo esc_html($address) ?>
                </p>
            <?php endif; ?>
            
            
        </div>
        <div class="col-md-3 price-section" style="text-align: center;">
            <div class="ct-ifs">
            <?php if(!empty( $info_price['price_new'] ) and $info_price['price_new']>0) { ?>
                <span class="booking-item-price-from"><?php st_the_language('tour_from') ?></span>
            <?php } ?>
            <?php 
                if (empty($tour_id)) {
                    $tour_id  = get_the_ID();
                }
            ?>
            <?php echo STTour::get_price_html($tour_id); ?>
            <span class="booking-item-price-from"><?php echo __('1 day', ST_TEXTDOMAIN); ?></span>
            </div>
            <a href="<?php echo esc_url($url)?>">
                <span class="btn btn-primary btn_book" style="background: #a3a3a3; border-color: #a3a3a3;border-radius: 30px;    padding: 10px 35px;"><?php st_the_language('tour_book_now') ?></span>
            </a>
            
            <?php if(!empty( $info_price['discount'] ) and $info_price['discount']>0 and $info_price['price_new'] >0) { ?>
                <?php echo STFeatured::get_sale($info_price['discount']); ?>
            <?php } ?>
        </div>
    </div>
</li>