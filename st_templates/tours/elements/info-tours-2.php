<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Tours info
 *
 * Created by ShineTheme
 *
 */

$style = $attr['style'];
if(empty($style))
    $style = '2';
wp_enqueue_script( 'st-qtip' );

//check is booking with modal
$st_is_booking_modal = apply_filters('st_is_booking_modal',false);

$type_tour = get_post_meta(get_the_ID(),'type_tour',true);

$max_people = get_post_meta(get_the_ID(), 'max_people', true);
$max_select = 0;
if($max_people == '' || $max_people == '0' || !is_numeric($max_people)){
    $max_select = 20;
}else{
    $max_select = $max_people;
}

$default = array('font_size'=> "3" , 'title1'=> __("Tour Informations" , ST_TEXTDOMAIN ) , 'title2'=> __("Place Order" , ST_TEXTDOMAIN) )  ;
extract(wp_parse_args( $attr, $default ));

$tour_booking_period = get_post_meta(get_the_ID(), 'tours_booking_period', true);
$date= new DateTime();
if($tour_booking_period){
	if($tour_booking_period==1) $date->modify('+1 day');
	else $date->modify('+'.($tour_booking_period).' days');
}
?>
<div id="booking-request"></div>
<?php 

    $tour_show_calendar = st()->get_option('tour_show_calendar', 'on');
    $tour_show_calendar_below = st()->get_option('tour_show_calendar_below', 'off');
    if($tour_show_calendar == 'on' && $tour_show_calendar_below == 'off'):
?>
<div class='tour_show_caledar_below_off'>
<?php echo st()->load_template('tours/elements/tour_calendar'); ?>
</div>
<?php endif; ?>

<div class="package-info-wrapper packge-info-wrapper-style2" style="width: 100%">
    <div class="overlay-form"><i class="fa fa-refresh text-color"></i></div>
    <div class="row">
        <div class="col-md-6">
            <h<?php echo esc_attr($font_size );?>><?php echo esc_attr($title2)?></h<?php echo esc_attr($font_size );?>>
		    <?php echo STTemplate::message(); ?>
            <form id="form-booking-inpage" method="post" action="#booking-request" class="mt10">
                <input type="hidden" name="style" value="<?php echo $style; ?>" />
                <input type="hidden" name="action" value="tours_add_to_cart" >
                <input type="hidden" name="item_id" value="<?php echo get_the_ID()?>">
                <input type="hidden" name="type_tour" value="<?php echo esc_html($type_tour) ?>">
                <div class="div_book">
				    <?php $check_in = STInput::request('check_in', ''); ?>
				    <?php $check_out = STInput::request('check_out', ''); ?>
				    <?php
				    if($tour_show_calendar == 'on'):
					    ?>
                        <!--Start - End datepicker-->
                        <div class="input-daterange" data-date-format="<?php echo TravelHelper::getDateFormatJs(); ?>" data-booking-period="<?php echo esc_attr($tour_booking_period) ?>" data-period="<?php echo esc_attr($date->format(TravelHelper::getDateFormat())) ?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mt10">
                                    <div class="form-group form-group-icon-left form-group-filled mb0">
                                        <label for="field-resort-checkin"><b><?php echo __('Start date', ST_TEXTDOMAIN); ?></b></label>
                                        <i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                        <input id="field-resort-checkin" data-post-id="<?php echo get_the_ID(); ?>" placeholder="<?php echo __('Select date', ST_TEXTDOMAIN); ?>" class="form-control checkin_tour" value="<?php echo STInput::post('check_in', STInput::get('check_in')); ?>" name="check_in" autocomplete="off" type="text">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
								    <?php if($style == '1'){ ?>
                                        <div class="form-group form-group-icon-left mb0 mt10">
                                            <label for="field-resort-checkout"><b><?php echo __('End date', ST_TEXTDOMAIN); ?></b></label>
                                            <i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                            <input id="field-resort-checkout" data-post-id="<?php echo get_the_ID(); ?>" placeholder="<?php echo __('Select date', ST_TEXTDOMAIN); ?>" class="form-control checkout_tour" value="<?php echo STInput::post('check_out', STInput::get('check_out')); ?>" name="check_out" autocomplete="off" type="text">
                                        </div>
								    <?php }else{ ?>
                                        <div class="form-group mb0 mt10">
                                            <label for="field-resort-modality"><b><?php echo __('Choice of modality', ST_TEXTDOMAIN); ?></b></label>
                                            <select class="form-control choice-pass-name" name="pass_day">
                                                <option value=""><?php echo __('--- Select ---') ?></option>
                                            </select>
                                        </div>
								    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--End Start - End datepicker-->
				    <?php else: ?>
                        <!--Start - End datepicker-->
                        <div class="input-daterange" data-date-format="<?php echo TravelHelper::getDateFormatJs(); ?>" data-booking-period="<?php echo esc_attr($tour_booking_period) ?>" data-period="<?php echo esc_attr($date->format(TravelHelper::getDateFormat())) ?>">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 mt10">
                                    <div class="form-group form-group-icon-left form-group-filled mb0">
                                        <label for="field-resort-checkin"><b><?php echo __('Start date', ST_TEXTDOMAIN); ?></b></label>
                                        <i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                        <input id="field-resort-checkin" data-post-id="<?php echo get_the_ID(); ?>" placeholder="<?php echo __('Select date', ST_TEXTDOMAIN); ?>" class="form-control checkin_tour" value="<?php echo STInput::post('check_in', STInput::get('check_in')); ?>" name="check_in" autocomplete="off" type="text">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">

								    <?php if($style == '1'){ ?>
                                        <div class="form-group form-group-icon-left mb0 mt10">
                                            <label for="field-resort-checkout"><b><?php echo __('End date', ST_TEXTDOMAIN); ?></b></label>
                                            <i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                            <input id="field-resort-checkout" data-post-id="<?php echo get_the_ID(); ?>" placeholder="<?php echo __('Select date', ST_TEXTDOMAIN); ?>" class="form-control checkout_tour" value="<?php echo STInput::post('check_out', STInput::get('check_out')); ?>" name="check_out" autocomplete="off" type="text">
                                        </div>
								    <?php }else{ ?>
                                        <div class="form-group  mb0 mt10">
                                            <label for="field-resort-modality"><b><?php echo __('Choice of modality', ST_TEXTDOMAIN); ?></b></label>
                                            <select class="form-control choice-pass-name" name="pass_day">
                                                <option value=""><?php echo __('--- Select ---') ?></option>
                                            </select>
                                        </div>
								    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <!--End Start - End datepicker-->
                        <div id="list_tour_item" data-type-tour="<?php echo $type_tour; ?>" style="display: none; width: 500px; height: auto;">
                            <div id="single-tour-calendar">
							    <?php echo st()->load_template('tours/elements/tour_calendar'); ?>
                                <style>
                                    .qtip{
                                        max-width: 250px !important;
                                    }
                                </style>
                            </div>
                        </div>
				    <?php endif; ?>

				    <?php
				    /**
				     * @since 2.0.0
				     * Add select starttime for tour booking
				     * Check starttime tour booking
				     * Half single layour
				     */
				    $starttime_value = STInput::request('starttime_tour', '');
				    ?>

                    <input type="hidden" data-starttime="<?php echo $starttime_value; ?>" data-checkin="<?php echo $check_in; ?>" data-checkout="<?php echo $check_out; ?>" data-tourid="<?php echo get_the_ID(); ?>" id="starttime_hidden_load_form" />
                    <div class="mt10" id="starttime_box" <?php echo $starttime_value != '' ? '' : 'style="display: none;"' ?>>
                        <strong><?php _e('Start time',ST_TEXTDOMAIN)?>: </strong>
                        <select class="form-control st_tour_starttime" name="starttime_tour" id="starttime_tour"></select>
                    </div>

                    <div class="row mt10">

					    <?php if(get_post_meta(get_the_ID(),'hide_adult_in_booking_form',true) != 'on'): ?>
                            <div class="col-xs-12 col-sm-6 mb10">
                                <strong><?php _e('Adults',ST_TEXTDOMAIN)?>: </strong>
                                <select class="mt10 form-control st_tour_adult" name="adult_number" required>
								    <?php for($i = 0; $i <= $max_select; $i++){
									    $is_select = '';
									    if (!empty(STInput::request('adult_number'))) {
										    if(STInput::request('adult_number') == $i) {
											    $is_select = 'selected="selected"';
										    }
									    }else{
										    if($i == 1){
											    $is_select = 'selected="selected"';
										    }
									    }
									    echo  "<option {$is_select} value='{$i}'>{$i}</option>";
								    } ?>
                                </select>
                            </div>
					    <?php endif;?>

					    <?php if(get_post_meta(get_the_ID(),'hide_children_in_booking_form',true) != 'on'): ?>
                            <div class="col-xs-12 col-sm-6 mb10">
                                <strong><?php _e('Children',ST_TEXTDOMAIN)?>: </strong>
                                <select class="mt10 form-control st_tour_children" name="child_number" required>
								    <?php for($i = 0; $i <= $max_select; $i++){
									    $is_select = '';
									    if(STInput::request('child_number') == $i){
										    $is_select = 'selected="selected"';
									    }
									    echo  "<option {$is_select} value='{$i}'>{$i}</option>";
								    } ?>
                                </select>
                            </div>
					    <?php endif;?>

					    <?php if(get_post_meta(get_the_ID(),'hide_infant_in_booking_form',true) != 'on'): ?>
                            <div class="col-xs-12 col-sm-6 mb10">
                                <strong><?php _e('Young',ST_TEXTDOMAIN)?>: </strong>
                                <select class="mt10 form-control st_tour_infant" name="infant_number" required>
								    <?php for($i = 0; $i <= $max_select; $i++){
									    $is_select = '';
									    if(STInput::request('infant_number') == $i){
										    $is_select = 'selected="selected"';
									    }
									    echo  "<option {$is_select} value='{$i}'>{$i}</option>";
								    } ?>
                                </select>
                            </div>
					    <?php endif;?>

					    <?php if(get_post_meta(get_the_ID(),'hide_senior_in_booking_form',true) != 'on'): ?>
                            <div class="col-xs-12 col-sm-6 mb10">
                                <strong><?php _e('Senior',ST_TEXTDOMAIN)?>: </strong>
                                <select class="mt10 form-control st_tour_senior" name="senior_number" required>
								    <?php for($i = 0; $i <= $max_select; $i++){
									    $is_select = '';
									    if(STInput::request('senior_number') == $i){
										    $is_select = 'selected="selected"';
									    }
									    echo  "<option {$is_select} value='{$i}'>{$i}</option>";
								    } ?>
                                </select>
                            </div>
					    <?php endif;?>

					    <?php if(get_post_meta(get_the_ID(),'hide_baby_in_booking_form',true) != 'on'): ?>
                            <div class="col-xs-12 col-sm-6 mb10">
                                <strong><?php _e('Baby',ST_TEXTDOMAIN)?>: </strong>
                                <select class="mt10 form-control st_tour_baby" name="baby_number" required>
								    <?php for($i = 0; $i <= $max_select; $i++){
									    $is_select = '';
									    if(STInput::request('baby_number') == $i){
										    $is_select = 'selected="selected"';
									    }
									    echo  "<option {$is_select} value='{$i}'>{$i}</option>";
								    } ?>
                                </select>
                            </div>
					    <?php endif;?>
                    </div>
				    <?php  $extra_price = get_post_meta(get_the_ID(), 'extra_price', true); ?>
				    <?php if(is_array($extra_price) && count($extra_price)): ?>
					    <?php $extra = STInput::request("extra_price");
					    if(!empty($extra['value'])){
						    $extra_value = $extra['value'];
					    }
					    ?>
                        <label><?php echo __('Extra', ST_TEXTDOMAIN); ?></label>
                        <table class="table">
						    <?php foreach($extra_price as $key => $val): ?>
                                <tr>
                                    <td width="80%">
                                        <label for="field-<?php echo $val['extra_name']; ?>" class="ml20 mt5"><?php echo $val['title'].' ('.TravelHelper::format_money($val['extra_price']).')'; ?>
										    <?php
										    if(isset($val['extra_required'])){
											    if($val['extra_required'] == 'on') {
												    echo '<small class="stour-required-extra" data-toggle="tooltip" data-placement="top" title="' . __('Required extra service', ST_TEXTDOMAIN) . '">(<span>*</span>)</small>';
											    }
										    }
										    ?>
                                        </label>
                                        <input type="hidden" name="extra_price[price][<?php echo $val['extra_name']; ?>]" value="<?php echo $val['extra_price']; ?>">
                                        <input type="hidden" name="extra_price[title][<?php echo $val['extra_name']; ?>]" value="<?php echo $val['title']; ?>">
                                    </td>
                                    <td width="20%">
                                        <select  style="width: 100px" class="form-control app" name="extra_price[value][<?php echo $val['extra_name']; ?>]" id="field-<?php echo $val['extra_name']; ?>">
										    <?php
										    $max_item = intval($val['extra_max_number']);
										    if($max_item <= 0) $max_item = 1;
										    $start_i = 0;
										    if(isset($val['extra_required'])) {
											    if ($val['extra_required'] == 'on') {
												    $start_i = 1;
											    }
										    }
										    for($i = $start_i; $i <= $max_item; $i++):
											    $check = "";
											    if(!empty($extra_value[$val['extra_name']]) and $i == $extra_value[$val['extra_name']]){
												    $check = "selected";
											    }
											    ?>
                                                <option <?php echo esc_html($check) ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
										    <?php endfor; ?>
                                        </select>
                                    </td>
                                </tr>
						    <?php endforeach; ?>
                        </table>
				    <?php endif; ?>
                    <input type="hidden" name="adult_price" id="adult_price">
                    <input type="hidden" name="child_price" id="child_price">
                    <input type="hidden" name="infant_price" id="infant_price">
                    <input type="hidden" name="senior_price" id="senior_price">
                    <input type="hidden" name="baby_price" id="baby_price">
                    <div class="message_box mb10"></div>
                    <div class="div_btn_book_tour">
					    <?php if($st_is_booking_modal){

						    ?>

                            <a data-target="#tour_booking_<?php the_ID() ?>" onclick="return false" class="btn btn-primary btn-st-add-cart" data-effect="mfp-zoom-out" ><?php st_the_language('book_now') ?> <i class="fa fa-spinner fa-spin"></i></a>
					    <?php }else{ ?>
						    <?php echo STTour::tour_external_booking_submit();?>
					    <?php } ?>
					    <?php echo st()->load_template('user/html/html_add_wishlist',null,array("title"=>'','class'=>'')) ?>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6"> 
            <h<?php echo esc_attr($font_size );?>><?php echo esc_attr($title1) ;?></h<?php echo esc_attr($font_size );?>>
            <div class="package_info_2 item mt20">
                <div class="title"><i class="fa fa-info-circle"></i></div>
                <div class="head">
			        <?php _e('Resort name',ST_TEXTDOMAIN) ?>:
                    <span class='text-color text-uppercase'>
                        <?php
                        echo get_the_title();
                        ?>
                    </span>
                </div>
            </div>
            <!--<div class="package_info_2 item mt20">
                <div class="title"><i class="fa fa-user-plus"></i></div>
                <div class="head">
                    <?php /*_e('Maximum People',ST_TEXTDOMAIN) */?>:
                    <?php /*$max_people = get_post_meta(get_the_ID(),'max_people', true) */?>
                    <span class='text-color text-uppercase'>
                        <?php /*
                            if( !$max_people || $max_people == 0 ){
                                $max_people = __('Unlimited', ST_TEXTDOMAIN);
                            }
                            echo esc_html($max_people) 
                        */?>
                    </span>
                </div>
            </div>-->
            <div class="package_info_2 item mt20">
                <div class="title"><i class="fa fa-map-marker"></i></div>
                <div class="head">
                    <?php _e('Location',ST_TEXTDOMAIN) ?>: 
                    <span class='text-color text-uppercase'>
                        <?php echo TravelHelper::locationHtml(get_the_ID()); ?>
                    </span>
                </div>
            </div>
            <!--<div class="package_info_2 item mt20">
                <div class="title"><i class="fa fa-star"></i></div>
                <div class="head">
                    <?php /*_e('Rate',ST_TEXTDOMAIN) */?>:
                    <ul class='text-color'>
                        <?php /*
                    $avg = STReview::get_avg_rate();
                    echo TravelHelper::rate_to_string($avg);
                    */?>
                    </ul>
                </div>
            </div>-->
            <div class="package_info_2 item mt20">
                <div class="title"><i class="fa fa-usd"></i></div>
                <div class="head">
                    <?php _e('Resort price',ST_TEXTDOMAIN) ?>:
                    <span id="ct-resort-price">---</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    if($tour_show_calendar == 'on' && $tour_show_calendar_below == 'on'):
?>
<div class='tour_show_caledar_below_on'>
<?php echo st()->load_template('tours/elements/tour_calendar'); ?>
</div>
<?php endif; ?>
<?php
if($st_is_booking_modal){?>
    <div class="mfp-with-anim mfp-dialog mfp-search-dialog mfp-hide" id="tour_booking_<?php echo get_the_ID()?>">
        <?php echo st()->load_template('tours/modal_booking');?>
    </div>

<?php }?>