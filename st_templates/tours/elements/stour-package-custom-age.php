<div class="custom-hotel-data-item">
    <table class="wp-list-table widefat fixed striped stour-list-custom-hotel" data-type="<?php echo $age; ?>">
        <thead>
        <tr>
            <!--<td class="manage-column column-cb check-column"></td>-->
            <td><?php echo __('Pass name (days)', ST_TEXTDOMAIN); ?></td>
            <td><?php echo __('Start date', ST_TEXTDOMAIN); ?></td>
            <td><?php echo __('Price', ST_TEXTDOMAIN); ?></td>
        </tr>
        </thead>
        <tbody>
        <tr class="parent-row">
            <!--<td>
                <a href="#del-item" class="hotel-del">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </td>-->
            <td><input type="text" class="pass-name" value="" readonly/></td>
            <td class="type-date-picker"><input type="text" class="pass-date st-datepicker" value="" data-date_format="<?php echo TravelHelper::getDateFormatJs(); ?>"/></td>
            <td><input type="number" class="pass-price" value=""/></td>
        </tr>
        <?php
        if(!empty($pack) and is_array($pack)) {
            foreach ($pack as $k => $v) {
                ?>
                <tr>
                    <!--<td>
                        <a href="#del-item" class="hotel-del">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    </td>-->
                    <td><input type="text" class="pass-name" value="<?php echo $v->pass_name; ?>" readonly=""/></td>
                    <td><input type="text" class="pass-date st-datepicker" value="<?php echo $v->pass_date; ?>" data-date_format="<?php echo TravelHelper::getDateFormatJs(); ?>"/></td>
                    <td><input type="number" class="pass-price" value="<?php echo $v->pass_price; ?>"/></td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>
