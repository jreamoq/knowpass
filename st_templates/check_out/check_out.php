<?php
    /**
     * @package    WordPress
     * @subpackage Traveler
     * @since      1.0
     *
     * Check out
     *
     * Created by ShineTheme
     *
     */

?>
<?php wp_nonce_field( 'traveler_order', 'st_security' ); ?>

<?php
    $booking_form = st()->load_template( 'hotel/booking_form', false, [
        'field_coupon' => false
    ] );

    $card_type = 'skidata';

    echo apply_filters( 'st_booking_form_billing', $booking_form );
    $cart = STCart::get_carts();
    $number_adult = 0;
    $number_child = 0;
    $number_young = 0;
    $number_senior = 0;
    $number_baby = 0;
    if(!empty($cart)){
        $cart_temp = array_shift($cart);
        $card_type = trim(get_post_meta($cart_temp['data']['st_booking_id'], 'st_card_type', true));
        $number_adult = $cart_temp['data']['adult_number'];
        $number_child = $cart_temp['data']['child_number'];
        $number_young = $cart_temp['data']['infant_number'];
        $number_senior = $cart_temp['data']['senior_number'];
        $number_baby = $cart_temp['data']['baby_number'];
    }

    $arr_guest_fields = array(
        'adult' => array(
            'name' => __('Adult', ST_TEXTDOMAIN),
            'number' => $number_adult,
        ),
        'child' => array(
            'name' => __('Children', ST_TEXTDOMAIN),
            'number' => $number_child,
        ),
        'young' => array(
            'name' => __('Young', ST_TEXTDOMAIN),
            'number' => $number_young,
        ),
        'senior' => array(
            'name' => __('Senior', ST_TEXTDOMAIN),
            'number' => $number_senior,
        ),
        'baby' => array(
            'name' => __('Baby', ST_TEXTDOMAIN),
            'number' => $number_baby,
        ),
    );

?>
<?php if ( defined( 'ICL_LANGUAGE_CODE' ) and ICL_LANGUAGE_CODE ): ?>
    <input type="hidden" name="lang" value="<?php echo esc_attr( ICL_LANGUAGE_CODE ) ?>">
<?php endif; ?>

<?php do_action( 'st_booking_form_field' ) ?>
<div class="id-option" style="width: 100%;">
    <h4>
        <?php
        switch ($card_type){
            case 'skidata':
	            echo __('Already have a skidata card?', ST_TEXTDOMAIN);
                break;
            case 'teamaxess':
	            echo __('Already have a teamaxess card?', ST_TEXTDOMAIN);
                break;
            case 'voucher':
	            echo __('Write the name of each skier', ST_TEXTDOMAIN);
                break;
        }
        ?>
    </h4>
    <div class="select-menu guest-check-out">
        <?php if($card_type == 'skidata' or $card_type == 'teamaxess'){ ?>
        <div style="margin-left: 20px;">
            <input style="float: left;" type="radio" id="select-option-yes" class="yes i-check check-has-guest" checked name="check_has_guest" value="yes">
            <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-yes"><?php echo __('YES', ST_TEXTDOMAIN); ?></label>
            <input style="float: left;" type="radio" id="select-option-no" class="no i-check check-has-guest" name="check_has_guest" value="no">
            <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-no"><?php echo __('NO', ST_TEXTDOMAIN); ?></label><br/>
        </div>
        <?php }else{
            ?>
            <div style="margin-left: 20px; display: none !important;">
                <input style="float: left;" type="radio" id="select-option-yes" class="yes i-check check-has-guest" name="check_has_guest" value="yes">
                <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-yes"><?php echo __('YES', ST_TEXTDOMAIN); ?></label>
                <input style="float: left;" type="radio" id="select-option-no" class="no i-check check-has-guest" checked name="check_has_guest" value="no">
                <label style="width: 100px;float: left;padding-left: 10px;" for="select-option-no"><?php echo __('NO', ST_TEXTDOMAIN); ?></label><br/>
            </div>
            <?php
        } ?>
        <div class="menu-content1" style="width: 100%;    padding-top: 20px; display: block !important">

            <?php
            foreach ($arr_guest_fields as $k => $v) {
                ?>
                <div class="filed-<?php echo $k; ?>">
                    <div class="">
                        <?php if (!empty($v['number'])) {
                            echo '<p><b>' . $v['name'] . '</b></p>';
                            for ($i = 0; $i < $v['number']; $i++) {
                                ?>
                                <div class="mb10">
                                <input style="height: 35px; width: 35%;" type="text" placeholder="<?php echo $v['name'] . ' ' . __('name', ST_TEXTDOMAIN) . ' ' . ($i + 1); ?>" name="st_guest[<?php echo $k; ?>][name][<?php echo $i; ?>]" autocomplete="off">
                                <input style="height: 35px; width: 50%;" type="text" placeholder="<?php echo $v['name'] . ' ' . __('card number', ST_TEXTDOMAIN) . ' ' . ($i + 1); ?>"
                                       name="st_guest[<?php echo $k; ?>][card][<?php echo $i; ?>]" class="name-card <?php echo ($card_type == 'voucher') ? 'hidden' : ''; ?>" autocomplete="off">
                                </div>
                                <?php
                            }
                            echo '<hr>';
                        } ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<div class="payment_gateways">
    <?php
        if ( !isset( $post_id ) ) $post_id = false;
        STPaymentGateways::get_payment_gateways_html( $post_id ) ?>
</div>
<div class="clearfix">
    <div class="row">
        <div class="col-sm-6">
            <?php if ( st()->get_option( 'booking_enable_captcha', 'on' ) == 'on' ) {
                $code = STCoolCaptcha::get_code();
                ?>
                <div class="form-group captcha_box">
                    <label for="field-hotel-captcha"><?php st_the_language( 'captcha' ) ?></label>
                    <img alt="<?php echo TravelHelper::get_alt_image(); ?>"
                         src="<?php echo STCoolCaptcha::get_captcha_url( $code ) ?>" align="captcha code"
                         class="captcha_img">
                    <input id="field-hotel-captcha" type="text" name="<?php echo esc_attr( $code ) ?>" value=""
                           class="form-control">
                    <input type="hidden" name="st_security_key" value="<?php echo esc_attr( $code ) ?>">
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php echo STCart::get_default_checkout_fields( 'st_check_create_account' ); ?>
<?php echo STCart::get_default_checkout_fields( 'st_check_term_conditions' ); ?>
<?php
    $cart = base64_encode( serialize( $cart ) );
?>
<input type="hidden" name="st_cart" value="<?php echo esc_attr( $cart ); ?>">
<div class="alert form_alert hidden"></div>
<a href="#" onclick="return false"
   class="btn btn-primary btn-st-checkout-submit btn-st-big "><?php _e( 'Submit', ST_TEXTDOMAIN ) ?> <i class="fa fa-spinner fa-spin"></i></a>

