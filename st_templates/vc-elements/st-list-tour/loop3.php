<div class="row row-wrap custom-list-tour-3">
    <?php
    while(have_posts()):
        the_post();
        if(intval($st_tour_of_row) <= 0 ) $st_tour_of_row =4;
        $col = 12 / $st_tour_of_row;

        $info_price = STTour::get_info_price();
        ?>
        <div class="col-md-<?php echo esc_attr($col) ?> style_box col-sm-6 col-xs-12 st_fix_<?php echo esc_attr($st_tour_of_row); ?>_col st_lazy_load">
            <?php echo STFeatured::get_featured(); ?>
            <div class="thumb" style="background-color: #f0f0f0; padding-bottom: 20px;">
                <header class="thumb-header">
                    <?php if(!empty( $info_price['discount'] ) and $info_price['discount']>0 and $info_price['price_new'] >0) { ?>
                        <?php echo STFeatured::get_sale($info_price['discount']); ?>
                    <?php } ?>
                    <a href="<?php the_permalink() ?>" class="hover-img">
                        <?php
                        /*$img = get_the_post_thumbnail( get_the_ID() , array(260,190,'bfi_thumb'=>true), array('alt' => TravelHelper::get_alt_image(get_post_thumbnail_id( )))) ;
                        if(!empty($img)){
                            echo balanceTags($img);
                        }else{
                            echo '<img width="800" height="600" alt="no-image" class="wp-post-image" src="'.bfi_thumb(ST_TRAVELER_URI.'/img/no-image.png',array('width'=>800,'height'=>600)) .'">';
                        }*/
                        TravelHelper::getLazyLoadingImage(array(800,600,'bfi_thumb'=>true));
                        ?>
                        
                    </a>
                </header>
                <div class="thumb-caption">
                    <div class="row mt10">
                        <?php if($location=TravelHelper::locationHtml(get_the_ID())){ ?>
                            <div class="col-md-6 col-sm-6 col-xs-6" style="width: 100%;text-align: center; color: #6684f2;">
                            
                                <?php
                                echo ($location);
                                ?>
                            </div>
                        <?php }?>
                        
                    </div>
                    <h5 class="hover-title hover-hold" style="text-align: center;padding-top: 13px; font-weight: bold; font-size: 19px;">
                            <?php the_title(); ?>
                    </h5>
                    <div class="row mt10">
                        <div class="col-md-6 col-sm-6 col-xs-6" style="width: 100%;text-align: center;">
                            <p class="mb0 text-darken">
                                
                                <?php echo STTour::get_price_html(false,false,' <br> -'); ?><?php _e(' /1 day',ST_TEXTDOMAIN) ?>
                            </p>
                        </div>
                        
                    </div>
                    <div class="row mt10">
                        
                        <div class="col-md-6 col-sm-6 col-xs-6" >
                            <ul class="icon-group text-color" style="padding-left: 70px;color: #6684f2;">
                                <?php echo  TravelHelper::rate_to_string(STReview::get_avg_rate()); ?>
                            </ul>
                            
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6" >
                            <p class="text-default"><?php comments_number(st_get_language('no_review'),st_get_language('based_on_1_review'),st_get_language('based_on_s_review'));?></p>
                        </div>
                        
                    </div>
                    <div class="row mt10">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right" style="width: 100%;text-align: center;">
                           <a href="<?php the_permalink() ?>" type="button" class="btn btn-primary" style="background: #a3a3a3; border-color: #a3a3a3;border-radius: 30px;    padding: 10px 35px;"><?php _e('Book Now',ST_TEXTDOMAIN) ?></a>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>

    <?php
    endwhile;
    ?>

</div>