<?php

/**

 * Created by wpbooking.

 * Developer: nasanji

 * Date: 3/14/2017

 * Version: 1.0

 */

?>

<?php

if(!empty($list_destinations['destination']['image'])){
    $image_id = $list_destinations['destination']['image'];
    $alt = TravelHelper::get_alt_image($image_id);
    ?>
    <div class="destination-map">
        <?php
        echo wp_get_attachment_image($image_id, 'full', array('class' => 'img-responsive', 'alt' => $alt));

        if(!empty($list_destinations['destination']['data_pos'])){
            foreach($list_destinations['destination']['data_pos'] as $key => $val){
                $top = isset($val['top'])?$val['top']:0;
                $left = isset($val['left'])?$val['left']:0;
                $tax = isset($val['tax'])?$val['tax']:0;
                $location_css = Assets::build_css('top: calc('.number_format($top,2).'% - 83px); left: calc('.number_format($left,2).'% - 33px)');
                $tax_link = '';
                if(!empty($tax)) {
                    $tax_link = get_the_permalink($tax);
                }
                $tax_marker = get_post_meta($tax, 'icon_map', true);
                $has_marker = '';
                if(!empty($tax_marker)){
                    $has_marker = 'tax-marker-image';
                }
                ?>
                <div class="location <?php echo esc_attr($location_css); ?> <?php echo esc_attr($has_marker); ?>">
                    <div class="oceaus-relative">
                        <a href="<?php echo esc_url($tax_link)?>" class="default-marker">
                            <div class="caption">
                                <span class="name"><?php echo get_the_title($tax); ?></span>
                            </div>
                        </a>
                        <span class="pulse"></span>
                        <span class="count"><?php echo TourHelper::countTourByLocationID($tax); ?></span>
                        <?php
                        if(!empty($tax_marker)){
                            $img = $tax_marker;
                            $background = Assets::build_css('background-image: url('.$img.')');
                            ?>
                            <div class="location-hover <?php echo esc_attr($background); ?>">
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
        <?php
            }
        }
        ?>
    </div>
<?php
}
?>
<div class="description row">
    <div class="col-md-6 col-sm-12 col-md-offset-3">
        <p><?php echo do_shortcode($description); ?></p>
    </div>
</div>
<?php
if(!empty($list_destinations['destination']['data_pos'])){
    ?>
    <div class="list-destination">
        <ul>
            <?php
            if(is_array($list_destinations['destination']['data_pos'])){
                foreach($list_destinations['destination']['data_pos'] as $key => $val){
                    if(!empty($val['tax'])){
                        $tax = $val['tax'];
                        $tax_count = TourHelper::countTourByLocationID($tax);
                        if(!empty($tax)){
                            echo '<li class="item"><span><a href="'.esc_url(get_the_permalink($tax)).'">'.get_the_title($tax).'</a>&nbsp;&nbsp; ( '.$tax_count.' )</span></li>';
                        }
                    }
                }
            }
            ?>
        </ul>
    </div>
    <div class="destination-link">
        <?php
        if (!empty($button_link['url']) && $button_link['url'] != '#') {
            echo '<a class="btn btn-default all-destinations" target="' . esc_attr($button_link['target']) . '" rel="' . esc_attr($button_link['rel']) . '" title="' . esc_attr($button_link['title']) . '" href="' . esc_url($button_link['url']) . '">' . esc_attr($button_name) . '</a>';
        } else {
            echo '<a class="btn btn-default btn-primary" href="#">' . esc_attr($button_name) . '</a>';
        }
        ?>
    </div>
<?php
}
?>

